#ifndef FILEHANDLING_H
#define FILEHANDLING_H

#include <QObject>
#include <QFile>
class FileHandling : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString currentFile READ currentFile WRITE setcurrentFile NOTIFY currentFileChanged)
    Q_PROPERTY(QString errorstring READ errorstring WRITE seterrorstring NOTIFY errorstringChanged)
public:
    explicit FileHandling(QObject *parent = nullptr);
    QString currentFile();
    QString errorstring();
private:
    QString FilePath;
    QFile File;
    QString Errorstring;
signals:
    void currentFileChanged();
    void errorstringChanged();
public slots:
    QString readFile(QString);
    void setcurrentFile(QString);
    void seterrorstring(QString);
    bool saveFile(QString,QString=NULL);
};

#endif // FILEHANDLING_H
