import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.14
import QtQuick.Dialogs 1.3
import QtQuick.Layouts 1.3
import "utility.js" as Utility
import Ajay 1.0
ApplicationWindow {
    property var tabobject:({})
    property var stackobject:({})
    property var filename:""
    property var fileurl: ""
    property string closefilename:""
    id:mainwindow
    visible: true
    width: 640
    height: 480
    title: qsTr(filename+" "+fileurl)
    menuBar:MenuBar {
        id: menubar
        width: parent.width
        background: Rectangle {
            color: "#CDCDCD"
            opacity: 0.3
        }
        Menu{
            id: openMenu
            title:qsTr("open")
            MenuItem{
                text:qsTr("file name1\npath")
            }
            MenuItem{
                text:qsTr("file name3\npath")
            }
            MenuItem{
                text:qsTr("file name2\npath")
            }
            MenuItem{
                text: qsTr("Other Documents...")
                onClicked: {
                    filedialogid.open()
                }
            }
        }
        Action {
            id: newAction
            property var i:1
            icon.name: "New"
            icon.source: "qrc:/icons/oNew.png"
            shortcut: StandardKey.New
            onTriggered: {
                Utility.createNewTab("Untiteld Documnet "+(++i));
            }
        }
        Action {
            id: saveAction
            icon.name: "Save"
            icon.source: "qrc:/icons/oSave.png"
            shortcut: StandardKey.Save
            onTriggered: {
                Utility.save()
            }
        }
        Action {
            id: moreAction
            icon.name: "More-Save as"
            icon.source: "qrc:/icons/oMore.png"
            onTriggered: {
                moreMenu.popup(recttemp)
                //open a menubar here later
            }
        }


        MenuBarItem{
            id :menuBarItemNew
            action: newAction
            background: Rectangle
            {
                id:tempid
                implicitWidth: 40
                radius: 7
                implicitHeight: 40
                opacity: menuBarItemNew.highlighted ? 1 : 0.3
                color:  "#CDCDCD"
            }
        }
        MenuBarItem{
            id :menuBarItemSave
            action: saveAction
            enabled: stacklayoutid.children[stacklayoutid.currentIndex].textEdited
            background: Rectangle {
                implicitWidth: 40
                radius: 10
                implicitHeight: 40
                opacity: menuBarItemSave.highlighted ? 1 : 0.4
                color:  "#CDCDCD" /*menuBarItem.highlighted ? "#F7F7F7" :*/
            }
        }

        MenuBarItem{
            id :menuBarItemMore
            //text:"Save as"
            action: moreAction
            background: Rectangle {
                id:recttemp
                implicitWidth: 40
                radius: 10
                implicitHeight: 40
                opacity: menuBarItemMore.highlighted ? 1 : 0.3
                color:  "#CDCDCD"
            }
        }

        MenuBarItem{
            id:searchbar
            visible: false
            contentItem:SearchBar{
                id:bar ;focusalias:false}
            }
}
    Action{
        id:findAction
        shortcut: StandardKey.Find
        onTriggered: {
            searchbar.visible=true
            bar.focusalias=true
        }
    }

    Menu{
        id:moreMenu
        property alias neXt: saveallid.next
        property bool saveallstatus: false
        MenuItem{
            text: "Save As"
            onClicked: {
                filedialogsave.open()
            }
        }
        MenuItem{
            id:saveallid
            text: "Save All"
            property var next:1
            property var initialindex: 0
            onNextChanged: {
                if(--toolbar.currentIndex>-1){
                    Utility.save()
                }
                else{
                    parent.saveallstatus=false
                    toolbar.currentIndex=initialindex
                }
            }
            onClicked:{
                moreMenu.saveallstatus=true
                initialindex=toolbar.currentIndex
                toolbar.currentIndex=toolbar.count-1;
                Utility.save()
            }
        }
        MenuItem{
            action: findAction
            text: "Find.."
            onClicked: {
                searchbar.visible=true
            }
        }
        MenuItem{text:"Find and Replace.."}
        MenuItem{text:"Go to Line"}
        MenuItem{text:"View"}
        MenuItem{text:"Tools"}
        MenuItem{
            text:"close"
            onClicked: {
                dialogid.open()
            }
        }
        MenuItem{text:"close All"}
    }
    FileDialog{
        id:filedialogsave
        selectExisting: false
        onAccepted: {
            console.log("Saving To--> "+fileUrl)
            filehandler.saveFile(stacklayoutid.children[stacklayoutid.currentIndex].inputtext,filedialogsave.fileUrl)
            var temp=fileUrl.toString();
            toolbar.currentItem.tabfileurl=temp
            toolbar.currentItem.tabfilename= temp.substring(temp.lastIndexOf('/')+1,temp.length);
            Utility.setTitle(toolbar.currentItem.tabfilename,temp)
            if(moreMenu.saveallstatus){
                moreMenu.neXt++
            }
        }
        onRejected: {
            filedialogsave.close()
            if(moreMenu.saveallstatus){
                moreMenu.neXt++
            }
        }
    }
    FileDialog{
        id:filedialclose
        selectExisting: false
        onAccepted: {
            filehandler.saveFile(stackobject[closefilename].inputtext,fileUrl)
            tabobject[closefilename].destroy()
            stackobject[closefilename].destroy()
        }
        onRejected: {
            filedialclose.close()
            dialogid.close()
        }
    }

    header:
        TabBar {
        id: toolbar
        contentHeight: 40
        background: Rectangle {
            anchors.fill: parent
            color: "#5D5D5D"
        }
        onCurrentIndexChanged: {
            Utility.setTitle(toolbar.currentItem.tabfilename,toolbar.currentItem.tabfileurl)
        }
    }
    Dialog{
        id:errordialog
        width:550
        height:100
        title:"Error occured while opening the file "+filename
        standardButtons: Dialog.Ok
        Text {
            id: errdialogtext
            anchors.fill:parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: filehandler.errorstring
        }
        onAccepted: {
            tabobject[filename].destroy()
            stackobject[filename].destroy()
        }
    }

    FileDialog{
        id:filedialogid
        onAccepted: {
            var temp=fileUrl.toString();
            //console.log("fileurl------------ temp "+temp)
            var filename = temp.substring(temp.lastIndexOf('/')+1,temp.length);
            Utility.createNewTab(filename,temp);
        }
        onRejected: {
            //console.log("Nothing")
        }
    }
    StackLayout {
        id:stacklayoutid
        anchors.fill:parent
        currentIndex: toolbar.currentIndex
        onCurrentIndexChanged: {
            stacklayoutid.children[stacklayoutid.currentIndex].forceActiveFocus()
            console.log("currentIndex-----in stack "+currentIndex)
        }
    }
    Filehandler{
        id:filehandler
        onErrorstringChanged: {
            errordialog.open()
        }
    }
    Dialog {
        id: dialogid
        width:550
        height:100
        title:"Save changes to document "+filename+" before closing?"

        standardButtons: Dialog.Discard |Dialog.Cancel | Dialog.Save
        onAccepted: {
            if(tabobject[closefilename].tabfileurl!=null){
                console.log(tabobject[closefilename].tabfileurl)
                filehandler.saveFile(qsTr(stackobject[closefilename].inputtext),tabobject[closefilename].tabfileurl)
                tabobject[closefilename].destroy()
                stackobject[closefilename].destroy()

            }
            else{
                filedialclose.open()
            }

        }
        onRejected: {
            dialogid.close()
        }
        onDiscard: {
            //Two files with same name should not be opend because one will be destroyed-later make it path specific
            tabobject[closefilename].destroy()
            stackobject[closefilename].destroy()
            console.log("on discard")
        }

        Text {
            id: dialogtext
            anchors.fill:parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: qsTr("If you don't save changes made will be lost permanently")
        }

    }
    Component.onCompleted: {
        Utility.loadObjects()
        console.log("Window loaded "+stacklayoutid.currentIndex)
        Utility.createNewTab("Untitled Document 1")
        Utility.setTitle(toolbar.currentItem.tabfilename,toolbar.currentItem.tabfileurl)
        stacklayoutid.children[stacklayoutid.currentIndex].forceActiveFocus()
    }
}
//    Dialog{
//        id:dialogcloseall
//        width: 550
//        height: 200
//        Flickable{
//            anchors.
//            Rectangle{
//                anchors.centerIn: parent
//                id:flickid
//                width:parent.width
//                height: parent.height
//                clip:true
//                contentHeight:textid.implicitHeight
//                contentWidth: textid.implicitWidth
//                model:[]
//            }
//        }
//    }
