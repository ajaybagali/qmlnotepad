var tabcomponent
var stackcomponent

function loadObjects(){
    tabcomponent=Qt.createComponent("Toolbaritem.qml")
    stackcomponent=Qt.createComponent("TextEditor.qml")
}
//creates New tab
function createNewTab(filename,fileurl) {
    tabobject[filename]=tabcomponent.createObject(toolbar,{tabfilename:filename,tabfileurl:fileurl})
    stackobject[filename]=stackcomponent.createObject(stacklayoutid,{cursorpos:0,textEdited:false,inputtext:fileurl!=null?filehandler.readFile(fileurl):""})
    stackobject[filename].textEdited=false
    if (tabobject[filename]== null || stackobject[filename]==null) {
        console.log("Error creating new tab",filename,fileurl)

    }
    else{
        toolbar.currentIndex=toolbar.count-1;
        console.log("Created new tab ",filename,fileurl)
    }
}

//To open popup when radomly closing the tabs
function openpopup(file_name){
    closefilename=file_name;
    if(stackobject[closefilename].textEdited)
        dialogid.open();
    else{
        console.log("No change Destroying")
        tabobject[closefilename].destroy()
        stackobject[closefilename].destroy()
    }
}

function setTitle(file_name,file_url){
    console.log("file url set to "+file_url)
    filehandler.setcurrentFile(file_url)
    filename=file_name;
    if(file_url!=null)
        fileurl=file_url;
    else
        fileurl=""
}
function save(){
    if(stacklayoutid.children[stacklayoutid.currentIndex].textEdited){
        helper()
    }
    else if(moreMenu.saveallstatus){
        moreMenu.neXt++
    }
}
function saveas(){
    filedialogsave.open()
}
function helper(){
    if(toolbar.currentItem.tabfileurl!=null){
        console.log("Saving--> "+toolbar.currentItem.tabfileurl)
        filehandler.saveFile(qsTr(stacklayoutid.children[stacklayoutid.currentIndex].inputtext),toolbar.currentItem.tabfileurl);
        if(moreMenu.saveallstatus){
            moreMenu.neXt++
        }
    }
    else{
        filedialogsave.open()
    }
}

function getIndicesOf(searchStr) {
    var searchStrLen = searchStr.length;
    if (searchStrLen == 0) {
        return [];
    }
    var startIndex = 0, index=0, indices = [];
    while ((index = stacklayoutid.children[stacklayoutid.currentIndex].inputtext.indexOf(searchStr, startIndex)) > -1) {
        indices.push(index);
        console.log("index",index);
        startIndex = index + searchStrLen;
    }
    return indices;
}
