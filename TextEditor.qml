import QtQuick 2.0
import "utility.js" as Utility
FocusScope{
    property alias inputtext:textid.text
    property bool textEdited: false
    property alias  cursorpos: textid.cursorPosition
    signal changecolor(var index, var len)
    width: flickableid.width
    height: flickableid.height
    Flickable{
        id:flickableid
        width:parent.width
        height: parent.height
        clip:true
        contentHeight:textid.implicitHeight
        contentWidth: textid.implicitWidth
        flickableDirection:Flickable.HorizontalAndVerticalFlick

        //To Ensure text is visible when we type out of rext
        function ensureVisible(r)
        {
            if (contentX >= r.x)
                contentX = r.x;
            else if (contentX+width <= r.x+r.width)
                contentX = r.x+(r.width)-width;
            if (contentY >= r.y)
                contentY = r.y;
            else if (contentY+height <= r.y+r.height)
                contentY = r.y+r.height-height;
        }
        TextEdit{
            id:textid
            width:  textid.implicitWidth>parent.width ? implicitWidth:flickableid.width
            height: textid.implicitHeight>parent.height ? implicitHeight:flickableid.height
            text:qsTr("")
            cursorPosition: 0
            focus: true
            selectedTextColor: "black"
            selectionColor:"yellow"
            persistentSelection:true
            onTextChanged: {
                    textEdited=true
            }
            Keys.onPressed: {
                if ((event.key === Qt.Key_S)&&(event.modifiers & Qt.ControlModifier)){
                    if(event.modifiers & Qt.ShiftModifier){
                        console.log('Key Saveas was pressed ')
                        Utility.saveas()
                    }
                    else{
                        console.log("Key Save was pressed ")
                        Utility.save()
                    }
                }
            }
            onCursorRectangleChanged: flickableid.ensureVisible(cursorRectangle)
        }
    }
    onChangecolor: {
        textid.select(index,index+len)
}
}
