import QtQuick 2.0
import QtQuick.Controls 2.4
import "utility.js" as Utility
import Ajay 1.0
TabButton{
    height: parent.height
    id:control
    property var tabfilename:""
    property var tabfileurl:""
    text: qsTr(tabfilename)
    width:textid.implicitWidth+45
    contentItem: Text {
        id:textid
        text: control.text
        font: control.font
        color: "#F7F7F7"
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignVCenter
    }
    background: Rectangle {
        width:implicitWidth
        height: implicitHeight
        opacity: enabled ? 1 : 0.3
        color: "#3B3B3B"
        radius: 5
        Rectangle{
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin:5
            color: "#3B3B3B"
            height: parent.height-10
            width:height
            radius: 5
            Text {
                anchors.fill:parent
                text: qsTr("x")
                color: "#F7F7F7"
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }
            MouseArea{
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    Utility.openpopup(tabfilename)
                }
                onHoveredChanged: {
                    containsMouse?parent.color="black":parent.color="#3B3B3B"
                }
            }
        }
    }
}
