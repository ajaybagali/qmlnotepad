import QtQuick 2.0
import QtQuick.Controls 2.14
import "utility.js" as Utility
Item {
    id:searchbarrect
    //anchors.fill: parent
    //color: "#CDCDCD"
    implicitHeight: 30
    implicitWidth: 330
    property bool focusalias: false
    onFocusaliasChanged:  {
        searchtext.forceActiveFocus()
        //console.log("in focus"+id)
    }
    Button {
        id:searchicon
        implicitWidth: 30
        implicitHeight: 30
        //signal click
        property string strsearched: ""
        icon.source: "qrc:/icons/oSearch.png"
        anchors.left: parent.left
        anchors.rightMargin: 5
        onClicked: {
            strsearched=searchtext.text
            stacklayoutid.children[stacklayoutid.currentIndex].forceActiveFocus()
            console.log(strsearched)
            searchtext.indices=Utility.getIndicesOf(strsearched)
            if(searchtext.indices.length>0){
                movedown.enabled=true
                moveup.enabled=true
                searchtext.i =0
                stacklayoutid.children[stacklayoutid.currentIndex].changecolor(searchtext.indices[0],strsearched.length)
            }
            else{
                searchtext.i=-1
                movedown.enabled=false
                moveup.enabled=false
            }
        }
    }
    TextArea {
        height: 30
        anchors.left: searchicon.right
        id: searchtext
        property var indices: []
        property var i: -1
        text:""
        placeholderText: qsTr("search")
        font.pixelSize: 16
        anchors.right:searchbarnumber.left
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignVCenter
        persistentSelection:true
        Keys.onPressed: {
            if (event.key === Qt.Key_Enter ){
                //searchicon
            }
        }
    }
    TextArea{
        id:searchbarnumber
        readOnly:true
        width:implicitWidth
        focus: true
        // anchors.left: searchtext.right
        anchors.right: moveup.left
        text: (searchtext.i+1)+" of "+(searchtext.indices.length>0?searchtext.indices.length:0)
    }
    Button{
        id:moveup
        width: 30
        height: 30
        enabled: false
        anchors.right: movedown.left
        icon.source:"qrc:/icons/oUp.png"
        onClicked: {
            stacklayoutid.children[stacklayoutid.currentIndex].forceActiveFocus()
            if(searchtext.i>=1){
                stacklayoutid.children[stacklayoutid.currentIndex].changecolor(searchtext.indices[--searchtext.i],searchicon.strsearched.length)
                console.log(searchtext.indices[searchtext.i])
            }
        }
    }
    Button{
        id:movedown
        width: 30
        height: 30
        enabled: false
        anchors.right: searchbarclose.left
        icon.source: "qrc:/icons/oDown.png"
        onClicked: {
            stacklayoutid.children[stacklayoutid.currentIndex].forceActiveFocus()
            if(searchtext.i<searchtext.indices.length-1){
                //stacklayoutid.children[stacklayoutid.currentIndex].cursorpos=searchtext.indices[++searchtext.i]
                stacklayoutid.children[stacklayoutid.currentIndex].changecolor(searchtext.indices[++searchtext.i],searchicon.strsearched.length)
                console.log(searchtext.indices[searchtext.i])
            }
        }
    }
    Button{
        id:searchbarclose
        width: 30
        height: 30
        enabled: true
        anchors.right: parent.right
        icon.source: "qrc:/icons/oClose.png"
        onClicked: {
            searchbar.visible=false
        }
    }
}
