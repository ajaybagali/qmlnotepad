#include "filehandling.h"
#include <QDebug>
#include <QUrl>
#include <QException>
FileHandling::FileHandling(QObject *parent) : QObject(parent)
{

}
void FileHandling::setcurrentFile(QString filename){
    QUrl url(filename);
    FilePath=url.path();
    qDebug()<<"c++ "<<"Filepath set to "<<FilePath;
    File.setFileName(FilePath);
}
QString FileHandling::currentFile(){
    return FilePath;
}
QString FileHandling::readFile(QString filename){
    QUrl url;
    try{
        url.setUrl(filename);
        QFile Readfile(url.path());
        if(!Readfile.open(QFile::ReadOnly|QFile::Text)){
            seterrorstring("Error Opening File "+filename+" "+Readfile.errorString());
            emit errorstringChanged();
            qDebug()<<"Error Opening File--"<<filename<<" "<<Readfile.errorString();
            return NULL;
        }
        QTextStream result(&Readfile);
        QString Re=result.readAll();
        Readfile.close();
        return Re;
    }
    catch (QException Ex) {
        seterrorstring(Ex.what());
        emit errorstringChanged();
        qDebug()<<"Exception while reading the file"<<url.path()<<Ex.what();
    }
    catch (...) {
        seterrorstring("Unknow exception while reading "+url.path());
        emit errorstringChanged();
        qDebug()<<"Exception while reading the file "<<url.path();
    }
    return  NULL;
}
void FileHandling::seterrorstring(QString Str){
    Errorstring=Str;
}
QString FileHandling::errorstring(){
    return Errorstring;
}
bool FileHandling::saveFile(QString data,QString filename){
    try{
        setcurrentFile(filename);
        emit currentFileChanged();
        //File name has to be updated when we randomly click a close button because it will not change current index
        if(!File.open(QFile::WriteOnly | QFile::Truncate)){
            seterrorstring("Error Opening File "+filename+" "+File.errorString());
            emit errorstringChanged();
            qDebug()<<"Error Opening File "<<FilePath<<" in "<<File.openMode()<<" "<<File.errorString()<<"saving";
            return false;
        }
        QTextStream out(&File);
        out<<data;
        File.close();
        qDebug()<<"c++ File saved "<<FilePath<<data;
        return true;
    }
    catch (QException Ex) {
        seterrorstring(Ex.what());
        emit errorstringChanged();
        qDebug()<<"Exception while saving the file"<<Ex.what();
    }
    catch (...) {
        seterrorstring("Unknow exception while saving "+FilePath);
        emit errorstringChanged();
        qDebug()<<"Exception while saving the file "+FilePath;

    }
    return false;
}

